linker = ld
flags = felf64
compiler = nasm
.PHONY: clean

all: lib.o main.o dict.o
	$(linker) -o $@ $^
lib.o: lib.asm
	$(compiler) -$(flags) -o $@ $<
main.o: main.asm
	$(compiler) -$(flags) -o $@ $<
dict.o: dict.asm
	$(compiler) -$(flags) -o $@ $<
clean:
	rm *.o all
