%define val 0

%macro colon 2
	%ifid %2
		%2: 
			dq val
			%define val %2
	%else
		%error "first arg should be string"
		%fatal
	%endif
	%ifstr %1
		db %1, 0
	%else 
		%error "first argument does not match"
		%fatal
	%endif
%endmacro
