global find_word
extern string_equals
extern string_length

section .text

%define eight_bytes 8

find_word:
	.loop:    
    	push rsi
	    add rsi, eight_bytes
	    call string_equals
	    pop rsi
	    test rax, rax
	    jnz .end_found

	    mov rsi, [rsi]
	    test rsi, rsi
	    jz .end_not_found
	    jmp .loop

	.end_not_found:
	    xor rax, rax
	    ret

	.end_found:
	    add rsi, eight_bytes
	    ;push rsi
	    call string_length
	    ;pop rsi
	    add rax, rsi
	    inc rax
	    ret
