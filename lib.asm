section .text

 
global exit
global print_string
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit: 
    mov    rax, 60         
    syscall

; Принимает указатель на нуль-терминированную строку в rdi, возвращает её длину
string_length:
      mov rax, 0            
    .loop:
      cmp byte [rdi+rax], 0 ; проверка на нуль-терминатор
      je .end
      inc rax               ; инкрементрируем счетчик 
      jmp .loop
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  call string_length  ;  вычисляем длину строки
  push rsi ; callee-saved
  push rdi 
  mov     rsi, rdi ; адрес начала
  mov     rdx, rax ; длина строки
  mov     rax, 1   ; syscall
  mov     rdi, 1   ; write
  syscall
  pop rdi  ; callee-saved
  pop rsi  
  ret

; Принимает код символа в rdi и выводит его в stdout
print_char:
  push rsi  ; callee-saved 
  push rdi
  mov     rsi, rsp
  mov     rdx, 1   
  mov     rax, 1 
  mov     rdi, 1
  syscall
  pop rdi   ; callee-saved 
  pop rsi
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
    mov rdi, 0xA 
    call print_char
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    
    mov r8, 0             
    mov r10, 10     
    mov rax, rdi          
    .num:
        inc r8            
        mov rdx, 0        
        div r10                            
        add rdx, 0x30       ; ASCII код символа
        push rdx           
        cmp rax, 0          ; проверка на конец слова
        jz .print           ; вывод
        jmp .num            ; следующая цифра     
    .print:
        pop rdi           
        call print_char     
        dec r8              
        cmp r8, 0          
        jnz .print          
        ret                

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                  ; проверка знака
    js .negative
.positive:         
    call print_uint             ; ипользуем print_uint
    ret
.negative:
    push rdi
    mov rdi, 0x2D               ; ASCII код символа "-" 
    call print_char             ; вывод
    pop rdi 
    neg rdi 
    call print_uint   
    ret

; Принимает два указателя в rdi и rsi на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        call string_length   ; проверка длины строки
        mov r10, rax         
        push rdi
        mov rdi, rsi
        call string_length   ; проверка длины строки
        pop rdi
        cmp rax, r10          
        jnz .result_0        ; не совпадают
        dec r10              ; совпадают
    .loop:
        cmp r10, 0           ; проверка символов
        jl .result_1         ; совпали
        mov cl, byte [rsi + r10]
        cmp cl, byte [rdi + r10]
        jnz .result_0        ; не совпали
        dec r10
        jmp .loop
    .result_1:
        mov rax, 1
        ret
    .result_0:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi                ; callee-saved
    push rsi
    push 0                
    mov rax, 0              ; syscall read
    mov rdi, 0               
    mov rsi, rsp            ; считываемый символ
    mov rdx, 1              ; длина строки
    syscall
    mov rax, [rsp]          ; введенный символ в rax
        pop rdx             
        pop rsi             ; callee-saved
        pop rdi
        ret  

; Принимает: адрес начала буфера в rdi, размер буфера в rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    .skip:
        call read_char          
        cmp rax, 0x09                ; проверка на табуляцию
        je .skip         
        cmp rax, 0x0A                ; проверка на символ перевода строки
        je .skip      
        cmp rax, 0x20                ; проверка на пробел
        je .skip       
        mov rdx, 0                   
    .loop:
        cmp rdx, rsi                 
        je .overflow_error                                         
        cmp rax, 0              ; проверка на нуль-терминатор
        je .end
        cmp rax, 0x09           ; проверка на табуляцию
        je .end        
        cmp rax, 0x0A           ; проверка на символ перевода строки
        je .end      
        cmp rax, 0x20           ; проверка на пробел
        je .end       
        mov byte [rdi + rdx], al
        inc rdx
        push rdx               
        call read_char          ; следующий символ
        pop rdx
        jmp .loop     ; в начало цикла    
    .overflow_error:
        mov rax, 0              ; возвращаем 0 в rax
        ret
    .end:
        mov byte [rdi + rdx], 0 ; добавляем нуль-терминатор
        mov rax, rdi            ; возвращаем адрес начала буффера
        ret
 
; Принимает указатель на строку в rdi, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    mov rbx, 0              
    mov r10, 10           
    mov rax, 0
    mov rdx, 0              ; счетчик колличества символов
    .loop:         
        mov bl, byte [rdi + rdx]  ; считывание символа
        cmp bl, 0x30              ; ASCII коды 0x30 - 0x39     
        jl .end                   ; проверка на валидность символов                
        cmp bl, 0x39            
        jg .end
        sub bl, 0x30 ; заменяем ASCII код числа самим числом
        push rdx
        mul r10      ; умножаем нынешнее число на 10
        pop rdx
        add rax, rbx ; прибавляем считанный символ
        inc rdx      ; инкрементируем счетчик
        jmp .loop        
    .end:
        pop rbx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        push rdi
    .check_sign:
        mov rax, rdi            
        cmp byte [rax], 0x2D      ; проверка на "-"
        jz .neg                   ; отрицательное число 
        call parse_uint           
        jmp .end            
    .neg:
        inc rdi                   ; указатель на строку сдвигаем на 1
        call parse_uint           
        cmp rdx, 0                
        jz .end                   ; возвращаем 0
        inc rdx                   ; добавляем 1 к считанным символам (знак "-")
        neg rax                   ; число представляем как отрицательное
        jmp .end    
    .end:
        pop rdi
        ret

; Принимает указатель на строку в rdi, указатель на буфер в rsi и длину буфера в rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        call string_length
        cmp rax, rdx
        jg .length_0
        mov rcx, 0    
    .loop:
        cmp rcx, rdx; 
        jge .length;  
        mov r11, [rdi + rcx]
        mov [rsi + rcx], r11
        inc rcx; rcx++
        jmp .loop   
    .length:
        mov rax, rdx
        ret
    .length_0: 
        mov rax, 0
        ret
    
