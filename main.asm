%include "colon.inc"

extern read_word
extern print_string
extern find_word
extern string_length
extern exit

section .rodata

%include "words.inc"
	err: db 'error:can not find your data', 0xA, 0
	too_long: db 'error:data is too long', 0xA, 0
buffer: times 255 db 0

section .text

%define eight_bytes 8

global _start

_start:
	mov rdi,buffer
	call read_word
	test rax, rax
	je .too_long
	
	
	call .find_word_func
	test rax, rax
	je .err
	
.find_word_by_key:
	mov rdi,rax
	add rdi, eight_bytes
	;push rdi
	;call string_length
	;pop rdi
	;add rdi,rax
	add rdi, rdx
	inc rdi
	call print_string
	xor rdi, rdi
	call exit
	
.find_word_func:
	mov rsi,val
	mov rdi,rax
	call find_word
	ret

.too_long:
    mov rdi, too_long
	jmp func
	mov rsi, too_long
	syscall
	call exit

.err:
    mov rdi, err
	jmp func
	mov rsi, err
	syscall
	call exit

func:
    call string_length
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	ret
